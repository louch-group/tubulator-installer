# Tubulator installer
Includes web installer for Matlab R2021a run time environment

Version 2.2 with multiple options for thresholding the images.
See Help menu document for details on how to use the Tubulator.

